import {App as StdApp} from "@std/app";
import Vue from "vue"
import AppComponent from '@main/vue/AppJs.vue'

export class App extends StdApp {
    constructor(config = {}) {
        super(config);
    }
    
    initVue() {
        Vue.config.productionTip = false

        new Vue({
            render: h => h(AppComponent),
        }).$mount("#app")
    }

    loadDom() {
        const preloader =  document.querySelector('#preloader');
        preloader && preloader.classList.add('hid');
    }

    run() {
        window.addEventListener('load', this.loadDom);
        this.initVue()
    }
}

import "@main/components/menu";
import "@main/components/header";
import "@main/components/modal";
import "@main/components/select-dropdown";
import "@main/components/input-filled";
import "@main/components/date-picker";
import "@main/components/sliders";
import "@main/components/toggle-slider";
import "@main/components/auto-height";
import "@main/components/tabs";
import "@main/components/copy";
import "@main/components/charts";
import "@main/components/toggle-active";
import "@main/components/tooltip";
import "@main/components/cookies";
import "@main/components/form-validation";
import "@main/components/operations-history";
import "@main/components/link-social";
import "@main/components/masked-input";
import "@main/components/question-answer";
import "@main/components/scroll-smooth";
import "@main/components/form";

