import axios from "./instance";
import rewriteUrl from "./middleware/rewrite-url";
import handleError from "./middleware/handle-error";

axios.interceptors.request.use(rewriteUrl, handleError);

export default axios;
