const MapComponents = {
    'map-canton': () => import('./map-canton'),
    'map-city': () => import('./map-city'),
    'map-lang': () => import('./map-lang'),
    'map-resort': () => import('./map-resort')
}

const MapStates = {
    0 : 'canton',
    1 : 'resort',
    2 : 'city',
    3 : 'lang',
}

export {
    MapStates, MapComponents
}
