/**
 * set attribute node
 *
 * @param {object} node
 * @param {object} style
 */
export const setStyleNode = (node, {attribute = {}}) =>
    Object.entries(attribute).forEach(([key, value]) => node.setAttribute(key, value))

/**
 * Set attribute nodes
 *
 * @param {object} node
 * @param {array<object>} nodes
 * @param {object} activeAttribute
 * @param {object} defaultAttribute
 */
export const assignAnActiveNode = (node, {activeAttribute = {}, defaultAttribute= {}, nodes = []}) =>
    setStyleNode(node, {attribute: node.getAttribute('fill') === activeAttribute.fill ? defaultAttribute : activeAttribute})
