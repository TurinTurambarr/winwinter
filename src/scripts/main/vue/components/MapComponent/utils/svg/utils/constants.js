const ActiveAttribute = {
    fill: '#E0ECF5',
    class: 'active',
    // stroke: '#000'
};

const DefaultAttribute = {
    fill: '#FFFFFF',
    class: 'no-active',
    // stroke: '#62A1CE'
};

export {
    ActiveAttribute,
    DefaultAttribute
}
