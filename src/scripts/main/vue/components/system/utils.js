export const LAST_STEP_ID = 777;
export const JSON = ['/ru/steps.json', '/ru/sell.json', '/ru/rent.json'];
export const BACK_URL = '/form';

export const getProperty = ([{variant = null}]) => variant
export const calculationComplexStep = ({step: {id, action = null, view = null}, action: variantAction, view: variantView, nextIdx: idx}) => {
    if(action == null && view == null) {
        return id === idx
    }

    const isCalculateNextStep = variantAction === action && variantView === view && id === idx

    if(isCalculateNextStep) {
        return variantAction === action && variantView === view && id === idx
    }

    const isLastStep = view === 'other';

    if(isLastStep) {
        return id === LAST_STEP_ID
    }

    return null
}

export const receiveExtremeOption = (steps, step, action = null, view = null) => {
    const path = {}
    for(let index = 0; index < steps.length; index++) {
        const s = steps[index]
    }
}
