import Vue from "vue";

export const UiInputProps = Vue.extend({
    props: {
        name: {
            type: String,
            required: true
        },
        label: {
            type: String,
        },
        type: {
            type: String,
            required: true
        },
        value: {
            type: [String, Number],
            default: ''
        },
        placeholder: {
            type: String,
            default: ''
        },
        idx: {
            type: Number,
            // required: true
        },
        isBorder: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        }
    }
})
