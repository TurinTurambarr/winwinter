import Vue from "vue";

export const WIDTH_VARIANT_LINE_PERCENT: number = 100
export const MARGIN_VARIANT_PERCENT: number = 0.8

export const VariantProps = Vue.extend({
    props: {
        item: {
            type: Object
        },
        idx: {
            type: Number
        },
        lastIdx: {
            type: Boolean,
            default: false
        },
        oneLine: {
            type: Boolean,
            default: false
        },
        length: {
            type: Number,
            default: null
        },
        isNoneMax: {
            type: Boolean,
            default: false
        },
        border: {
            type: Boolean,
            default: false
        },
        offLastLine: {
            type: Boolean,
            default: false
        }
    }
})
