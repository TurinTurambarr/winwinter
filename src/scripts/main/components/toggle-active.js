class ToggleActive {

    constructor(el) {
        this.el = el;
        this.initEvents();
    }

    initEvents() {

        this.el.addEventListener("click", () => {

            const isActive = this.el.classList.contains("active");
            this.toggleActive(isActive);
        });
    }

    toggleActive(isActive) {
        if (isActive) {
            this.el.classList.remove("active");
        } else {
            this.el.classList.add("active");
        }
    }
}

document.querySelectorAll(".js-toggle-active").forEach(elem => new ToggleActive(elem));
