class ScrollSmooth{

    constructor(el) {
        this.el = el;
        this.initEvents();
    }

    initEvents() {

        this.el.addEventListener("click", event => {
            this.scrollPage(event);
        });
    }

    scrollPage(event) {

        if(document.title==="Главная"){
            // console.log(document.title)
            // console.log(event);
            event.preventDefault();
            const blockId= event.target.getAttribute('href').substr(2);
            // console.log(event.target.getAttribute('href').substr(2)); 

            document.getElementById(blockId).scrollIntoView({
                behavior: "smooth",
                block: "start",
            })
        }
        
    }
}

document.querySelectorAll(".js-smooth-scroll").forEach(elem => new ScrollSmooth(elem));