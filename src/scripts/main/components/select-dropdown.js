class SelectDropdown {

    constructor(el) {
        this.el = el;
        this.btn = this.el.querySelector(".js-select-btn");
        this.input = this.el.querySelector("input");
        this.text = this.el.querySelector(".js-select-text");
        this.dropdown = this.el.querySelector(".js-select-dropdown");
        this.buttons = this.el.querySelectorAll(".js-select-button");
        this.initEvents();
    }

    initEvents() {

        this.btn.addEventListener("click", () => {
            this.toggleDropdown();
        });

        this.buttons.forEach((button) => {
            button.addEventListener("click", () => {
                this.selectOption(button);
            });
        });

        document.addEventListener("mouseup", (e) => {

            if (!e.target.classList.contains(".js-select-wrap") && !e.target.closest(".js-select-wrap")) {
                this.el.classList.remove("open");
            }
        });
    }

    toggleDropdown() {

        const isOpen = this.el.classList.contains("open");

        if(isOpen) {
            this.el.classList.remove("open");
        }else {
            this.el.classList.add("open");
        }
    }

    selectOption(button) {
        const val = button.dataset.value;
        const text = button.innerText;

        this.el.classList.remove("open");
        this.el.classList.add("selected");
        this.input.value = val;
        this.text.innerText = text;
    }
}

document.querySelectorAll(".js-select-wrap").forEach(elem => new SelectDropdown(elem));

