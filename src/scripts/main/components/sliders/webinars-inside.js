import Swiper, {Navigation, Pagination} from "swiper";
Swiper.use([Navigation, Pagination]);

class SliderWebinars {
    constructor(el) {
        this.el = el;
        this.paginationNode = this.el.querySelector(".js-slider-webinars-inside-pagination");                
        this.initSlider();                
    }

    initSlider() {
        let pagination = false;

        if(this.paginationNode) {
            pagination = {
                el: this.paginationNode,
                type: "fraction",
            };
        }        

        new Swiper(this.el, {
            slidesPerView: 1,  
            spaceBetween: 8, 
            pagination: pagination,                    
            navigation: {
                nextEl: this.el.querySelector(".js-slider-webinars-inside-nav-next"),
                prevEl: this.el.querySelector(".js-slider-webinars-inside-nav-prev"),
            },            
        });
    }
}

window.addEventListener("load", function() {
    document.querySelectorAll(".js-slider-webinars-inside").forEach(slider => new SliderWebinars(slider));
}, false);
