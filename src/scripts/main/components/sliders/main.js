import Swiper, {Navigation, Autoplay} from "swiper";
Swiper.use([Navigation, Autoplay]);

class SliderMain {
    constructor(el) {
        this.el = el;
        this.parent = this.el.parentElement; 
        this.autoplay = this.el.dataset.autoplay;       
        this.initSlider();
    }

    initSlider() {
        let autoplay = false;

        if(this.autoplay) {
            autoplay = {
                delay: 10000,
            }
        }

        new Swiper(this.el, {
            slidesPerView: 1,         
            spaceBetween: 8,
            loop: true, 
            autoplay: autoplay,       
            navigation: {
                nextEl: this.parent.querySelector(".js-slider-main-nav-next"),
                prevEl: this.parent.querySelector(".js-slider-main-nav-prev"),
            },
            breakpoints: {   
                768: {
                    spaceBetween: 16,
                },                
            }
        });
    }
}

window.addEventListener("load", function() {
    document.querySelectorAll(".js-slider-main").forEach(slider => new SliderMain(slider));
}, false);
