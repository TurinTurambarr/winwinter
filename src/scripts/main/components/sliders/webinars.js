import Swiper, {Navigation} from "swiper";
Swiper.use([Navigation]);

class SliderWebinars {
    constructor(el) {
        this.el = el;
        this.parent = this.el.parentElement;        
        this.initSlider();
    }

    initSlider() {
        new Swiper(this.el, {
            slidesPerView: 1,
            spaceBetween: 8,
            slidesOffsetAfter: 15,            
            navigation: {
                nextEl: this.parent.querySelector(".js-slider-webinars-nav-next"),
                prevEl: this.parent.querySelector(".js-slider-webinars-nav-prev"),
            },
            breakpoints: {
                768: {
                    slidesPerView: 2, 
                    spaceBetween: 16,                                     
                },
                1080: {
                    slidesPerView: 2, 
                    spaceBetween: 24, 
                    slidesOffsetAfter: 0,                           ё
                },
            }
        });
    }
}

window.addEventListener("load", function() {
    document.querySelectorAll(".js-slider-webinars").forEach(slider => new SliderWebinars(slider));
}, false);
