import Swiper, {Navigation} from "swiper";
Swiper.use([Navigation]);

class SliderLessons {
    constructor(el) {
        this.el = el;
        this.parent = this.el.parentElement;        
        this.initSlider();
    }

    initSlider() {
        new Swiper(this.el, {
            slidesPerView: 2,
            spaceBetween: 8,
            slidesOffsetAfter: 15,            
            navigation: {
                nextEl: this.parent.querySelector(".js-slider-lessons-nav-next"),
                prevEl: this.parent.querySelector(".js-slider-lessons-nav-prev"),
            },
            breakpoints: {
                768: {
                    slidesPerView: 3, 
                    spaceBetween: 16,                                     
                },
                1080: { 
                    slidesPerView: 3,                     
                    spaceBetween: 24,
                    slidesOffsetAfter: 0,                                      
                },
                1440: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                    slidesOffsetAfter: 0,                                                  
                },
            }
        });
    }
}

window.addEventListener("load", function() {
    document.querySelectorAll(".js-slider-lessons").forEach(slider => new SliderLessons(slider));
}, false);
