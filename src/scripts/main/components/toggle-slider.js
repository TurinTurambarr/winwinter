class ToggleSlider {

    constructor(el) {
        this.el = el;
        this.btns = this.el.querySelectorAll(".js-toggle-slider-btn");   
        this.sliders = this.el.querySelectorAll(".js-toggle-slider");      
        this.initEvents();
    }

    initEvents() {

        this.btns.forEach((btn) => {

            btn.addEventListener("click", () => {
                const value = btn.dataset.value;
                this.toggleSlider(value);
            });
        });        
    }

    toggleSlider(value) {

        this.sliders.forEach((slider) => {
            slider.classList.add("hidden");
        });

        this.el.querySelector(`.js-toggle-slider[data-value="${value}"]`).classList.remove("hidden");
    }    
}

document.querySelectorAll(".js-toggle-slider-wrap").forEach(elem => new ToggleSlider(elem));
