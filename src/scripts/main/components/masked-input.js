import IMask from "imask";

export function inputMaskInit() {

    const maskedInput = document.querySelectorAll(".js-masked-input");

    maskedInput.forEach((input) => {

        const maskNode = input.getAttribute("data-mask");
        IMask(input, {
            mask: maskNode,
        });
    });
}

document.addEventListener("DOMContentLoaded", () => {
    inputMaskInit();
});
