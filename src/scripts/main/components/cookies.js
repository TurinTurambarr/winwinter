document.addEventListener("DOMContentLoaded", () => {

    const cookies = document.querySelector(".js-cookies"),
        cookiesBtn = document.querySelector(".js-cookies-btn");

    if(cookies === undefined) {
        return;
    }

    if (getCookie("cookie") !== "accept") {
        cookies.classList.add("show");
    }

    cookiesBtn.addEventListener("click", () => {
        document.cookie = "cookie=accept";
        cookies.remove();
    });

    function getCookie(name) {
        /* eslint-disable */
        let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
        /* eslint-enable */
    }
});
