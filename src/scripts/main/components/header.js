class Header {

    constructor(el) {
        this.el = el;
        this.height = this.el.clientHeight;
        this.menu = document.querySelector(".js-menu");
        this.initEvents();
    }

    initEvents() {

        let scrollTop = window.scrollY;

        window.addEventListener("scroll", () => {

            if ((scrollTop > window.scrollY) && (window.scrollY > 0)) {
                this.fixHeader(window.scrollY);
            }
            else {
                this.unfixHeader(window.scrollY);
            }

            if(window.scrollY === 0) {
                this.el.classList.remove("header__bg-paddings");        
                this.el.style.boxShadow = "0px 6px 17px rgba(34, 60, 80, 0.0)";
            } else{
                this.el.classList.add("header__bg-paddings");
                this.el.style.boxShadow = "0px 6px 17px rgba(34, 60, 80, 0.2)";
                this.el.style.transition = "all .25s"; 
            }

            scrollTop = window.scrollY;
        });
    }

    fixHeader(scrollY) {

        if(scrollY <= this.height) {
            this.el.classList.remove("fixed");
        }

        this.el.classList.remove("fade-out");
        this.el.classList.add("fixed", "fade-in");
        this.el.classList.add("header__bg-paddings");  
        

        
    }

    unfixHeader(scrollY) {

        const isMenuOpened = document.querySelector("html").classList.contains("menu-opened");

        if(isMenuOpened) {
            return;
        }

        if(scrollY > this.height) {
            this.el.classList.remove("fade-in");
            this.el.classList.add("fade-out");
        }

        this.el.classList.remove("fixed");
    }
}

document.querySelectorAll(".js-header").forEach(elem => new Header(elem));
