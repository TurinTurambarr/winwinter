import axios from "@main/axios";

document.addEventListener("DOMContentLoaded", () => {

    let items = [];
    let offset = 0;
    const table = document.querySelector(".js-history-table");
    const tbody = document.querySelector(".js-history-table tbody");
    const btnMore = document.querySelector(".js-history-btn");
    const perPage = 10;
    const icons = [
        "check_round",
        "cross_round",
        "wallet_input"
    ];
    const statuses = [
        "Урок посещен",
        "Урок пропущен",
        "Пополнение"
    ];

    if (!table) {
        return;
    }

    btnMore.addEventListener("click", function() {
        getOperations()
    });

    function getOperations() {
        axios({
            url: routes.operations,
            params: {"offset": offset*perPage, "count": perPage},
            method: "GET",
        }).then(
            response => {
                const data = response.data.data
                items = data.items;
                offset++;
                items.forEach((item) => {
                    if(data.end) {
                        btnMore.remove();
                        return;
                    }
                    const row = item;
                    const day = row.date;
                    const time = row.time;
                    const balance = row.balance;
                    const icon = icons[row.status];
                    const status = statuses[row.status];
                    tbody.insertAdjacentHTML("beforeend", `<tr><td>${day}1</td><td>${time}</td>
                <td><svg class="operations-history__table-icon"><use href="#${icon}"></use></svg>${status}</td>
                <td>${balance}</td></tr>`);
                })
            }
        );
    }
});
