import Chart from "chart.js/auto";

document.addEventListener("DOMContentLoaded", () => {

    const chart = document.querySelectorAll(".js-chart-homework-progress");    

    chart.forEach((el) => {

        const ctx = el.getContext("2d");
        let data = el.dataset.data;            
        let dataArr;            

        if(data) {
            dataArr = data.slice(1,-1).split(",");
        }
        

        new Chart(ctx, {
            type: "doughnut",
            data: {
                datasets: [
                    {
                        backgroundColor: ["#262626", "#E9E9E9"],
                        hoverBackgroundColor: ["#262626", "#E9E9E9"],
                        borderWidth: 0,
                        data: dataArr,
                    
                    }
                ],                
            },
            options: {
                cutout: "80%",      
                tooltip: {
                    enabled: false,                    
                },
                legend: {
                    display: false,                    
                }, 
                events: [],               
            },
        });

    });

});
