export class linkSocial {

    constructor(el) {
        this.el = el;
        this.btn = this.el.querySelector(".js-form-btn");
        this.initEvents();
    }

    initEvents() {

        this.el.addEventListener("click", () => {

            if(this.el.classList.contains("btn-type5")) {
                this.el.classList.remove("btn-type5");
                this.el.classList.add("btn-type1");
                this.el.textContent = "Привязать";
            }else if(this.el.classList.contains("btn-type1")) {
                this.el.classList.remove("btn-type1");
                this.el.classList.add("btn-type5");
                this.el.textContent = "Отвязать";
            }
        })
    }
}

document.querySelectorAll(".js-button-social").forEach(elem => new linkSocial(elem));
