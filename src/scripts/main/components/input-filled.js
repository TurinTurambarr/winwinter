export class InputFilled {

    constructor(el) {        
        this.el = el;
        this.toggleEl = this.el.querySelector("input") || this.el.querySelector("textarea");        
        this.initEvents();    
    }

    initEvents() {        

        this.toggleClass();        

        this.el.addEventListener("change", () => {                        
            this.toggleClass();
        });        
    }    

    toggleClass() {         

        if(!this.toggleEl) {
            return;
        }        

        if(!this.toggleEl.value) {
            this.toggleEl.classList.remove("filled");
            return;
        }

        this.toggleEl.classList.add("filled");        
    }
}

document.querySelectorAll(".input-wrap").forEach(elem => new InputFilled(elem));
