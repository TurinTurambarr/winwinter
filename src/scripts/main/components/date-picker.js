import AirDatepicker from "air-datepicker"
export class DatePicker {

    constructor(el,) {
        this.el = el;
        this.initEvents();
    }

    initEvents() {

        const options = {
            inline: false,
            autoClose: false,
            navTitles: {
                days: "MMMM yyyy <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M8 10L12 14L16 10' stroke='#262626' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'/></svg>"
            },
            range: false,
            multipleDatesSeparator: " - ",
            prevHtml: `<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.88889 13.8889L5 9.99997M5 9.99997L8.88889 6.11108M5 9.99997L15 9.99997" stroke="#10171F" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>`,
            nextHtml: `<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.1111 6.11108L15 9.99997M15 9.99997L11.1111 13.8889M15 9.99997L5 9.99997" stroke="#10171F" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>`,
            position: "bottom right",
            onShow: () => {
                this.el.classList.add("filled");
            },
            onHide: (animationEnd) => {

                if(this.el.value.length || !animationEnd) {
                    return;
                }

                this.el.classList.remove("filled");
            },
            onSelect: (formattedDate) => {
                this.el.classList.add("filled");
                this.el.setAttribute("data-value", formattedDate);

                setTimeout(() => {
                    datepicker.hide();
                },1000);
            },
        };

        const datepicker = new AirDatepicker(this.el, options);

        this.el.addEventListener("keyup", () => {
            const value = this.el.value;

            if(value.length < 10) {
                return;
            }

            const [day, month, year] = this.el.value.split(".");
            const date = new Date(year, (month - 1), day);

            datepicker.selectDate(date);
        });
    }
}

document.querySelectorAll(".js-date-picker").forEach(elem => new DatePicker(elem));
