import ResponsiveAutoHeight from "responsive-auto-height";

document.addEventListener("DOMContentLoaded", () => {

    const autoheightNode = document.querySelectorAll(".js-auto-height");

    if(!autoheightNode.length) {
        return;
    }

    new ResponsiveAutoHeight(".js-auto-height");
});
