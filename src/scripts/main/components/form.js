import instance from "@main/axios";

const validateHandler  = {
    'length': (value, length) => {
        return value.length > length
    }
}

export class FormHandler {
    constructor(el) {
        this.el = el;
        this.inputs = this.el && this.el.querySelectorAll(".js-valid");
        this.backUrlEndpoint = '/form'

        this.initEvents()
    }

    initEvents() {
        this.el && this.el.addEventListener('submit', this.sendData.bind(this))
    }

    async sendData(e) {
        e.preventDefault();
        const cont = e;
        const {data = null, inputs = null} = cont;
        const {urlBack = null, messageSuccess = 'Сообщение отправленно', messageError = 'Произошла ошибка, попробуйте позже или свяжитесь с администратором'} = cont?.el?.dataset ?? {};

        const isValidate = this.validateForm(inputs)

        if(isValidate === false) {
            return;
        }

        const dataInputs = data != null ? {...data} : this.collectData(cont?.el);

        const {data: submitForm = {}} = await instance({
            method: 'post',
            url: urlBack || this.backUrlEndpoint,
            data: dataInputs
        })

        if(submitForm?.status === 'success') {
            this.visibleMessage(cont?.el, {message: messageSuccess, className: 'alert-success'})
            return;
        }

        this.visibleMessage(cont?.el, {message: messageError})
    }

    collectData(el = null) {
        if(el == null) {
            return;
        }

        return [...el.querySelectorAll('input, select, textarea')].reduce((acc, {name, value}) => {
            return {...acc, [name]: value}
        })
    }

    visibleMessage(input, {
        className = 'alert-error',
        message,
        time = 3000
    }) {
        if(input == null) {
            return
        }

        const div = document.createElement('div');
        div.className = className;
        div.textContent = message;



        input.after(div);
        setTimeout(() => div.remove(), time);
    }

    validateForm(inputs = null) {
        const fields = inputs != null ? [...inputs] : [...this.inputs];

        return fields.reduce((acc, input) => {
            const {test, message} = input.dataset;
            const {value} = input;

            const hasWhatTest  = test.split(",").reduce((accWhat, val) => {
                const [rule, params] = val.split(':');

                if(rule in validateHandler) {
                    const isValidate = validateHandler[rule](value, params);

                    if (isValidate === false) {
                        this.visibleMessage(input, {
                            message
                        })
                    }

                    return [
                        ...accWhat,
                        isValidate
                    ]
                }
            }, []).every(elem => elem === true);

            return [
                ...acc,
                hasWhatTest
            ];
        }, []).every(elem => elem === true);
    }
}

document.querySelectorAll("form").forEach(elem => new FormHandler(elem));
