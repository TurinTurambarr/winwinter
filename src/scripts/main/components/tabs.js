class Tabs {

    constructor(el) {
        this.el = el;
        this.tabs = this.el.querySelectorAll(".js-tab");
        this.wrap = this.el.closest(".js-tabs-wrap");
        this.items = this.wrap?.querySelectorAll(".js-tab-item");
        this.initEvents();
    }

    initEvents() {

        this.tabs.forEach((tab) => {

            tab.addEventListener("click", (e) => {
                this.toggleTabs(e);

                if(this.items) {
                    this.toggleItems(e);
                }                
            });
        });
    }

    toggleTabs(e) {

        this.tabs.forEach((tab) => {
            tab.classList.remove("active");
        });
        e.target.classList.add("active");
    }

    toggleItems(e) {

        const value = e.target.dataset.value;
        this.items.forEach((item) => {
            item.classList.add("hidden");
        });
        this.wrap.querySelector(`.js-tab-item[data-value="${value}"]`).classList.remove("hidden");
    }
}

document.querySelectorAll(".js-tabs").forEach(elem => new Tabs(elem));
