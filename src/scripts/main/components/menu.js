// import scrollLock from "./scroll-lock";

class Menu {

    constructor(el) {
        this.el = el;
        this.htmlNode = document.querySelector("html");
        this.toggleBtn = document.querySelectorAll(".js-menu-toggle");
        // this.breakpoint = window.matchMedia( "(min-width:768px)" );
        this.initEvents();
    }

    initEvents() {

        this.toggleBtn.forEach((btn) => {

            btn.addEventListener("click", () => {

                const isMenuOpened = this.htmlNode.classList.contains("menu-opened");

                if (isMenuOpened) {
                    this.closeMenu()
                } else {
                    this.openMenu();
                }
            });
        });

        document.addEventListener("mouseup", (e) => {

            // if (!this.breakpoint.matches) {
            //     return;
            // }

            if (!e.target.classList.contains(".js-menu-toggle") && !e.target.closest(".js-menu-toggle")) {
                this.closeMenu();
            }
        });
    }

    openMenu() {
        this.el.classList.add("fade-in");
        this.el.classList.remove("fade-out");
        this.el.style.display = "block";
        this.htmlNode.classList.add("menu-opened");

        // if (!this.breakpoint.matches) {
        //     scrollLock.enable();
        // }

        setTimeout(() => {
            this.el.classList.remove("fade-in");
        },250);
    }

    closeMenu() {
        this.el.classList.remove("fade-in");
        this.el.classList.add("fade-out");

        // if (!this.breakpoint.matches) {
        //     scrollLock.disable();
        // }

        this.htmlNode.classList.remove("menu-opened");

        setTimeout(() => {
            this.el.classList.remove("fade-out");
            this.el.style.display = "none";
        },250);
    }
}

document.querySelectorAll(".js-menu-modal").forEach(elem => new Menu(elem));
