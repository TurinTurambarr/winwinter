import tippy from "tippy.js";

const factory = (options) => ({
    ...{
        duration: 250,
        maxWidth: 500,
        placement: "bottom",
        allowHTML: true
    },
    ...options,
})

export class Tooltip {
    constructor(el, options = {}) {
        this.el = el;
        this.content = this.el.getAttribute("data-tooltip");
        this.options = {...factory({el: this.el, content: this.content}), ...options}
        this.initEvents();
    }

    initEvents() {
        tippy(this.el, this.options);
    }
}

document.querySelectorAll(".js-tooltip").forEach(elem => new Tooltip(elem));

