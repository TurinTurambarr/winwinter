import keycode from "keycode";
import scrollLock from "./scroll-lock";

class Modal {

    constructor(el) {
        this.el = el;
        this.type = this.el.dataset.modal;
        this.openBtn = document.querySelectorAll(`.js-modal-open[data-modal="${this.type}"]`);
        this.closeBtn = this.el.querySelectorAll(".js-modal-close");
        this.htmlNode = document.querySelector("html");
        this.initEvents();
    }

    initEvents() {

        this.openBtn.forEach((btn) => {
            btn.addEventListener("click", () => {
                this.openModal();
            });
        });

        this.closeBtn.forEach((btn) => {
            btn.addEventListener("click", () => {
                this.closeModal();
            });
        });

        document.addEventListener("keyup", (e) => {
            if(e.keyCode == keycode("Escape")) {
                this.closeModal();
            }
        });
    }

    openModal() {
        this.el.classList.add("fade-in");
        this.el.classList.remove("fade-out");
        this.el.style.opacity = "1";
        this.el.style.visibility = "visible";
        this.htmlNode.classList.add("modal-opened");
        scrollLock.enable();

        setTimeout(() => {
            this.el.classList.remove("fade-in");
        },250);
    }

    closeModal() {
        this.el.classList.remove("fade-in");
        this.el.classList.add("fade-out");
        this.htmlNode.classList.remove("modal-opened");
        scrollLock.disable();

        setTimeout(() => {
            this.el.classList.remove("fade-out");
            this.el.style.opacity = "0";
            this.el.style.visibility = "hidden";
        },250);
    }
}

document.querySelectorAll(".js-modal").forEach(elem => new Modal(elem));
