
class ToggleActive {

    constructor(el) {
        this.el      = el;
        this.answers = document.querySelectorAll(".question-answer__component")
        this.initEvents();
    }

    initEvents() {
        this.el.addEventListener("click", this.toggleActive.bind(this));
    }

    toggleActive() {
        this.answers.forEach(answer => answer.classList[answer === this.el ? 'add' : 'remove']("question-answer__component-active"))
    }
}

document.querySelectorAll(".question-answer__component").forEach(elem => new ToggleActive(elem));
