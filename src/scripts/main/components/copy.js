import ClipboardJS from "clipboard/dist/clipboard";

class Copy {

    constructor(el) {
        this.el = el;
        this.wrap = this.el.closest(".js-copy-wrap");        
        this.initEvents();
    }

    initEvents() {

        const clipboard = new ClipboardJS(this.el); 
        
        clipboard.on("success", () => {
            this.wrap.classList.add("copied");
        });        
        
    }
}
document.querySelectorAll(".js-copy").forEach(elem => new Copy(elem));
