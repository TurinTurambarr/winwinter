import Inputmask from "inputmask";
export class formValidation {

    constructor(el) {
        this.el = el;
        this.btn = this.el.querySelector(".js-form-btn");
        this.inputs = this.el.querySelectorAll("input:not([type=radio], .visually-hidden, .js-date-picker)");
        this.email = this.el.querySelectorAll("input[type=email]");
        this.phone = this.el.querySelectorAll("input[type=tel]");
        this.pass = this.el.querySelectorAll("input[type=password]");
        this.passPair = this.el.querySelectorAll(".js-input-pass-pair");
        this.numberCode = this.el.querySelectorAll(".js-input-number-code");
        this.formIsValid = true;
        this.initEvents();
    }

    initEvents() {

        this.numberCode.forEach(item => {
            item.addEventListener("focusout", event => {
                this.validateCode(event.target);
            })
        });

        this.pass.forEach(item => {
            item.addEventListener("focusout", event => {
                this.validatePass(event.target);
            })
        });

        this.email.forEach(item => {
            item.addEventListener("focusout", event => {
                this.validateEmail(event.target);
            })
        });
        

        // this.phone.forEach(item => {
        //     const inputWrap = item.closest(".input-wrap");
        //     Inputmask({"mask": "+7(999)999-99-99",
        //         "onincomplete": function() {
        //             if(item.value.length) {
        //                 inputWrap.classList.add("input-error");
        //             }
        //         },
        //         "oncomplete": function() {inputWrap.classList.remove("input-error");}}
        //     ).mask(item);
        // });

        this.passPair.forEach(item => {
            item.addEventListener("focusout", () => {
                this.validatePassPair();
            })
        });

        this.inputs.forEach(item => {
            item.addEventListener("keyup", () => {
                this.validateFilling();
            })
        });

        this.inputs.forEach(item => {
            item.addEventListener("focus", () => {
                item.closest(".input-wrap").classList.remove("input-error");
            })
        });

        this.btn.addEventListener("click", () => {
            this.validateFrom();
        })
    }

    validateFilling() {
        this.formIsFilled = true;
        this.inputs.forEach(item => {
            if (item.value.length<=0) {
                this.btn.setAttribute("disabled", "disabled");
                return;
            }
            this.btn.removeAttribute("disabled");
        });
    }

    validateFrom() {
        this.formIsValid = true;
        this.inputs.forEach(item => {
            if (item.value.length<=0) {
                this.formIsValid = false;
                alert("No!")
                return;
            }

            this.formIsValid = (item.closest(".input-wrap").classList.contains("input-error"))
                ? false : this.formIsValid;
        });

        if(!this.formIsValid) {
            this.btn.setAttribute("disabled", "disabled");
            return;
        }
        this.btn.removeAttribute("disabled");
    }

    validateCode(el) {
        const inputWrap = el.closest(".input-wrap");
        const codeLength = el.value.length;

        if(!inputWrap) {
            return;
        }

        inputWrap.classList.remove("input-error");

        if(codeLength<=0) {
            return;
        }

        if (codeLength !== 6) {
            inputWrap.classList.add("input-error");
        }
    }

    validatePass(el) {
        const inputWrap = el.closest(".input-wrap");
        const passLength = el.value.length;

        if(!inputWrap) {
            return;
        }
        inputWrap.classList.remove("input-error");

        if(passLength<=0) {
            return;
        }

        if (passLength<6) {
            inputWrap.classList.add("input-error");
        }
    }

    validatePassPair() {
        const passEl1 = this.passPair[0];
        const passEl2 = this.passPair[1];
        const pass1 = passEl1.value;
        const pass2 = passEl2.value;
        const inputWrap = passEl2.closest(".input-wrap");

        if(!inputWrap) {
            return;
        }
        inputWrap.classList.remove("input-error");

        if(pass1.length<=0 || pass2.length<=0) {
            return;
        }

        if (pass1 !== pass2) {
            inputWrap.classList.add("input-error");
        }
    }

    validateEmail(el) {
        /* eslint-disable */
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        /* eslint-enable */
        const inputWrap = el.closest(".input-wrap");
        const email = el.value;

        if(!inputWrap) {
            return;
        }

        inputWrap.classList.remove("input-error");

        if(email.length<=0) {
            return;
        }

        if(!re.test(String(email).toLowerCase())) {
            inputWrap.classList.add("input-error");
        }
    }
}

document.querySelectorAll(".js-form-validate").forEach(elem => new formValidation(elem));
