const merge = require("webpack-merge");
const path = require("path");
const dotenv = require("dotenv");

const base = require("./webpack.base");

const env = dotenv.config().parsed;

module.exports = merge(base, {
    devtool: "source-map",

    devServer: {
        before() {

        },
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: env.DEV_SERVER_PORT,
        writeToDisk: true,
        disableHostCheck: true,
        hot: false,
        inline: false,
        headers: {
            "Access-Control-Allow-Origin": "*",
        }
    },
});
